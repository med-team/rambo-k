package org.rki.readclassifier;

import java.io.InputStream;

public interface ReadClassifier {
	public String getName();
	public String getDescription();
	public void readData(InputStream in) throws Exception;
	public void getScore(ReadPair reads);
}
